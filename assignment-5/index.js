"use strict";

window.onload = function(){
    var calc = document.getElementById('calc')
    calc.onclick = function(){
        var loan = document.getElementById('loan').value
        var interest = document.getElementById('Interest').value
        var time = document.getElementById('years').value

        var loanAmount= parseInt(loan)
        var interestRate = parseInt(interest)/100
        var years = parseInt(time)

        const monthlyInterestRate = interestRate / 12;
        const numberOfPayments = years * 12;

        const monthlyPayment = (loanAmount * monthlyInterestRate * Math.pow((1 + monthlyInterestRate), numberOfPayments)) /(Math.pow((1 + monthlyInterestRate), numberOfPayments) - 1);

        const totalPayment = monthlyPayment * numberOfPayments;
        const totalInterest = totalPayment - loanAmount;
        
        
        var result = document.getElementById('result')
        result.innerHTML = `<table cellspacing="10" class="resulttable">
                                <tr>
                                    <td><caption>Results</caption></td>
                                </tr>
                                <tr>
                                    <td><label class="monthly">Monthly Payment</label><input type="text"  class="monthlypayment" value=${monthlyPayment} disabled/></td>
                                </tr>
                                <tr>
                                    <td><label class="total">Total Payment</label><input type="text"  class="resultpay" value=${totalPayment} disabled/></td>
                                </tr>
                                <tr>
                                    <td><label class="totalinterest">Total Interest</label><input type="text" class="resultint" value=${totalInterest} disabled/></td>
                                </tr>
                            </table>`
    }
}